package com.filipp.inextest.versions;

import java.lang.ref.SoftReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class VersionController {

    private List<Version> versions;

    public VersionController(List<Version> versions) {
        this.versions = versions;
    }

    public VersionController(String[] versions) {
        this.versions = Arrays.stream(versions).map(Version::new).collect(Collectors.toList());
    }

    public List<Version> getVersions() {
        return versions;
    }

    public List<Version> getSortedVersion(){
        return getSortedVersion(this.versions);
    }

    public List<Version> getSortedVersion(List<Version> versions){
        versions = new ArrayList<>(versions);

        versions.sort(Version::compareTo);
        return versions;
    }

}
