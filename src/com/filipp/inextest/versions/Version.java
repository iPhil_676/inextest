package com.filipp.inextest.versions;

public class Version implements Comparable<Version>{

    private int major;
    private int minor;
    private int bugfix;

    public Version(String version) {
        String[] splittedVersion = version.split("\\.");

        this.major = Integer.valueOf(splittedVersion[0]);
        this.minor = Integer.valueOf(splittedVersion[1]);
        this.bugfix = Integer.valueOf(splittedVersion[2]);
    }

    public Version(int major, int minor, int bugfix) {
        this.major = major;
        this.minor = minor;
        this.bugfix = bugfix;
    }

    public int getMajor() {
        return major;
    }

    public int getMinor() {
        return minor;
    }

    public int getBugfix() {
        return bugfix;
    }

    @Override
    public int compareTo(Version version) {
        int res;
        if((res = this.major - version.major) != 0)
            return res;

        if((res = this.minor - version.minor) != 0)
            return res;

        return this.bugfix - version.bugfix;
    }

    @Override
    public String toString(){
        return major + "." + minor + "." + bugfix;
    }
}
