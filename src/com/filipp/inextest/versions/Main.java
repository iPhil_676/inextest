package com.filipp.inextest.versions;

public class Main {

    public static void main(String[] args) {
        String[] versions = {"1.2.3", "2.4.31", "1.1.1", "5.232.42", "0.31313.421", "3.2.2", "3.2.3", "1.55.434"};

        VersionController versionController = new VersionController(versions);

        versionController.getSortedVersion().forEach(version -> System.out.println(version.toString()));
    }
}
