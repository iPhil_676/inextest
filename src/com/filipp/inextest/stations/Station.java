package com.filipp.inextest.stations;

public class Station {

    private String name;

    public Station(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Station) {
            Station anotherStation = (Station) obj;
            return name.equals(anotherStation.name);
        }
        return false;
    }

    @Override
    public String toString() {
        return name;
    }
}