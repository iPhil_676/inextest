package com.filipp.inextest.stations;

import java.util.*;

public class StationTree {
    private Map<Character, StationTree> map = new HashMap<>();

    private int index;

    private List<Station> stations;

    //Оставляем единственный публичный конструктор, который будет создавать "корневой" элемент с индексом 0
    public StationTree(List<Station> stations) {
        this.stations = new ArrayList<>();
        //Можно было бы и не указывать, но для наглядности
        this.index = 0;

        stations.forEach(this::add);
    }

    private StationTree(Station station, int index) {
        stations = new ArrayList<>();
        this.index = index;

        add(station);
    }

    private StationTree(int index) {
        stations = new ArrayList<>();
        this.index = index;
    }

    // Добавляем станцию, в случае необходимости создаём новый лист
    public void add(Station station){
        stations.add(station);
        if(station.getName().length() > index) {
            Character key = station.getName().charAt(index);
            map.computeIfAbsent(key, (k) -> new StationTree(index + 1)).add(station);
        }
    }

    // Сам метод поиска
    public List<Station> find(String prefix){
        // Сделаем проверку на то, что длина префикса не превышает индекс листа
        // Скорее нужно для дебага, чтобы убедиться, что дерево действительно строиться верно
        if(prefix.length() < index)
            throw new IllegalArgumentException("Длина префикса не может быть больше индекса листа");

        // Условие выхода: если индекс равен длине префикса, то мы нашли интересующий нас список
        if(index == prefix.length()) return stations;

        Character key = prefix.charAt(index);
        StationTree tree = map.get(key);
        //Проверим есть ли интересующий нас лист
        if(tree != null){
            return tree.find(prefix);
        }
        // Если нет, то вернём пустой список
        return new ArrayList<>();

    }
}
