package com.filipp.inextest.stations;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class StationTask {

    private static List<Station> STATION_LIST = Arrays.asList(
            new Station("МОСКВА"),
            new Station("МОЖГА"),
            new Station("МОЗДОК"),
            new Station("САНКТ-ПЕТЕРБУРГ"),
            new Station("САМАРА"));

    private StationTree stationTree;

    public static void main(String[] args) {
        StationTask task = new StationTask(STATION_LIST);
        System.out.println(task.getStationsByAnyFirstLetters("МО"));
        System.out.println(task.getStationsByAnyFirstLetters("СА"));
        System.out.println(task.getStationsByAnyFirstLetters("САМ"));
        System.out.println(task.getStationsByAnyFirstLetters("КАЗ"));
    }

    //Здесь инициализируется рут дерева станций с помощью единственного публичного конструктора
    private StationTask(List<Station> stationList) {
        stationTree = new StationTree(stationList);
    }

    //Поменял название метода, так как теперь он ищет не только по первым двум буквам, а по любому префиксу
    private Collection<Station> getStationsByAnyFirstLetters(String prefix) {
        return stationTree.find(prefix);
    }
}