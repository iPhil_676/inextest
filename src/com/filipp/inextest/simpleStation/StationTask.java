package com.filipp.inextest.simpleStation;

import java.util.*;

public class StationTask {

    private static List<Station> STATION_LIST = Arrays.asList(
            new Station("МОСКВА"),
            new Station("МОЖГА"),
            new Station("МОЗДОК"),
            new Station("САНКТ-ПЕТЕРБУРГ"),
            new Station("САМАРА"));

    private Map<String, List<Station>> map;

    public static void main(String[] args) {
        StationTask task = new StationTask(STATION_LIST);
        System.out.println(task.getStationsByTwoFirstLetters("МО"));
        System.out.println(task.getStationsByTwoFirstLetters("СА"));
    }

    private StationTask(List<Station> stationList) {
        map = new HashMap<>();
        //Создаём мульти карту, которая будет по ключу (первые две буквы названия станции) хранить лист станций
        stationList.forEach(station -> {
            map.computeIfAbsent(station.getName().substring(0, 2), (v) -> new ArrayList<>()).add(station);
        });
    }

    private Collection<Station> getStationsByTwoFirstLetters(String prefix) {
        //Вытаскиваем из мапы, в лучшем случае скорость будет О(1)
        return map.get(prefix);
    }

    private static class Station {

        private String name;

        public Station(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}